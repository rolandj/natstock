# NatStock -  natural language parser for visualising stock market data.

[Live version](https://rolandj.gitlab.io/natstock/)

![Demo image](https://media.giphy.com/media/d7kQmj9CNLpTkzpw29/giphy.gif)