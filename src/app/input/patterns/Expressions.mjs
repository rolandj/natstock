export const Expressions = {
    /* Regular expression patterns */
    keywords: {
        // Stock companies
        google: /google/gi,
        microsoft: /microsoft/gi,
        apple: /apple/gi,
        amazon: /amazon/gi,
        facebook: /facebook/gi,
        netflix: /netflix/gi,
        tesla: /tesla/gi,
        intel: /intel/gi,
        // Keywords
        and: / +and +/gi,
        to: / +to +/gi,
        compare: /compare/g,
        Compare: /Compare/g,
        show: /show/g,
        Show: /Show/g,
        // Date patterns
        day: /by +day/gi,
        year: /by +month/gi,
        week: /by +week/gi
    },
    /* HTML representations */
    colored: {
        // Companies
        google: '<span class="company"> google </span>',
        microsoft: '<span class="company"> microsoft </span>',
        apple: '<span class="company"> apple </span>',
        amazon: '<span class="company"> amazon </span>',
        facebook: '<span class="company"> facebook </span>',
        netflix: '<span class="company"> netflix </span>',
        tesla: '<span class="company"> tesla </span>',
        intel: '<span class="company"> intel </span>',
        // Keywords
        show: '<span class="instruction"> show </span>',
        Show: '<span class="instruction"> Show </span>',
        and: '<span class="instruction"> and </span>',
        to: '<span class="instruction"> to </span>',
        compare: '<span class="instruction"> compare </span>',
        Compare: '<span class="instruction"> Compare </span>',
        // Date patterns
        day: '<span class="time"> by day </span>',
        week: '<span class="time"> by week </span>',
        year: '<span class="time"> by month </span>'
    },
    /* Full expressions */
    grammar: {
        show: /((S|s)how\s+)(google|microsoft|apple|facebook|netflix|tesla|intel|amazon) +(by\s+day|by\s+week|by\s+month)/gi,
        compare: /((C|c)ompare\s+)(google|microsoft|apple|facebook|netflix|tesla|intel|amazon)\s+to\s+(google|microsoft|apple|facebook|netflix|tesla|intel|amazon) +(by\s+day|by\s+week|by\s+month)/gi,
        separator: /((\s+and\s+)|(\.))/gi
    }
};

export const replacements = new Map([
    /* Map keywords with their html representations */
    // Companies
    [Expressions.keywords.google, Expressions.colored.google],
    [Expressions.keywords.microsoft, Expressions.colored.microsoft],
    [Expressions.keywords.apple, Expressions.colored.apple],
    [Expressions.keywords.netflix, Expressions.colored.netflix],
    [Expressions.keywords.amazon, Expressions.colored.amazon],
    [Expressions.keywords.intel, Expressions.colored.intel],
    [Expressions.keywords.facebook, Expressions.colored.facebook],
    [Expressions.keywords.tesla, Expressions.colored.tesla],
    // Keywords
    [Expressions.keywords.day, Expressions.colored.day],
    [Expressions.keywords.week, Expressions.colored.week],
    [Expressions.keywords.year, Expressions.colored.year],
    [Expressions.keywords.show, Expressions.colored.show],
    [Expressions.keywords.Show, Expressions.colored.Show],
    [Expressions.keywords.compare, Expressions.colored.compare],
    [Expressions.keywords.Compare, Expressions.colored.Compare],
    [Expressions.keywords.and, Expressions.colored.and],
    [Expressions.keywords.to, Expressions.colored.to]
]);


export class Stock {
    /*
    Represent stock as object with specified name and time interval
    Further API calls are performed for these values
    */

    constructor(name = "", time = "") {
        this.time = this.get_time(time);
        this.name = this.get_name(name);
        this.getTimeMode = this.getTimeMode.bind(this);
        this.fetchApi = this.fetchApi.bind(this);
    };

    get_time(time) {
        if (Expressions.keywords.day.exec(time)) {
            Expressions.keywords.day.lastIndex = 0;
            return "TIME_SERIES_DAILY";
        }
        if (Expressions.keywords.week.exec(time)) {
            Expressions.keywords.week.lastIndex = 0;
            return "TIME_SERIES_WEEKLY";
        }
        if (Expressions.keywords.year.exec(time)) {
            Expressions.keywords.year.lastIndex = 0;
            return "TIME_SERIES_MONTHLY";
        }
    }

    get_name(name) {
        if (Expressions.keywords.google.exec(name)) {
            return "GOOG";
        }
        if (Expressions.keywords.microsoft.exec(name)) {
            return "MSFT";
        }
        if (Expressions.keywords.apple.exec(name)) {
            return "AAPL";
        }
        if (Expressions.keywords.facebook.exec(name)) {
            return "FB";
        }
        if (Expressions.keywords.netflix.exec(name)) {
            return "NFLX";
        }
        if (Expressions.keywords.amazon.exec(name)) {
            return "AMZN";
        }
        if (Expressions.keywords.tesla.exec(name)) {
            return "TSLA";
        }
        if (Expressions.keywords.intel.exec(name)) {
            return "INTC";
        }
    }

    /* Translate response time description */
    getTimeMode() {
        let mode = []
        switch (this.time) {
            case "TIME_SERIES_MONTHLY":
                mode = ["Monthly Time Series", ' MONTHS.'];
                break;
            case "TIME_SERIES_DAILY":
                mode = ["Time Series (Daily)", ' DAYS.'];
                break;
            case "TIME_SERIES_WEEKLY":
                mode = ["Weekly Time Series", ' WEEKS.'];
                break;
            default:
                break;
        }
        return mode;
    }

    async fetchApi() {
        const base = "https://www.alphavantage.co/query?function=";
        const url = `${base}${this.time}&symbol=${this.name}&apikey=AJP30PV8M68OHMP3`;
        let response = await fetch(url);
        return await response.json();
    }
}