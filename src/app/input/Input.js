import React, { Component } from 'react'
import { Expressions as regs, replacements, Stock } from './patterns/Expressions.mjs'
import './Input.css'


class Input extends Component {
    constructor(props) {
        super(props);
        this.newOffset = 0;
        this.currentOffset = 0;
        this.htmlOffset = 0;
        this.processInput = this.processInput.bind(this);
        this.findRegexps = this.findRegexps.bind(this);
        this.setCaretToEnd = this.setCaretToEnd.bind(this);
        this.colorTextInput = this.colorTextInput.bind(this);
    }

    render() {
        return (
            <div className="input-container">
                <div className="input" contentEditable="true" spellCheck="false" onKeyDown={this.processInput} />
                <h3 className="input-instruction">Type your commands here</h3>
                <h4 className="input-tip">Tip: space after "." or "and" triggers drawing chart. </h4>
            </div>
        );
    }

    processInput(e) {
        // First cut examined text to avoid scanning same input every time
        const text = e.target.textContent.slice(this.currentOffset, e.target.textContent.length);
        const stock = this.findRegexps(text);
        // If pattern matched save offset
        if (stock) {
            this.currentOffset += this.newOffset;
            // Color keywords
            const newHtmlInput = e.target.innerHTML.slice(this.htmlOffset, e.target.innerHTML.length);
            const previousHtmlInput = e.target.innerHTML.slice(0, this.htmlOffset);
            e.target.innerHTML = previousHtmlInput + this.colorTextInput(newHtmlInput);
            // Lift up found stock
            this.props.onPatternMatch(stock);
        }
        this.setCaretToEnd(e.target);
    }

    /* Match regular expressions */
    findRegexps(text) {
        let counter = 0;
        let matchedShow, matchedCompare, match, cutText = null;
        // First look for separator, which triggers matching expressions
        while ((match = regs.grammar.separator.exec(text))) {
            cutText = text.slice(counter, match.index);
            // Find keywords
            matchedShow = regs.grammar.show.exec(cutText);
            matchedCompare = regs.grammar.compare.exec(cutText);
            // Update counter
            counter = match.index;
            // Set newOffset if expression found
            if (matchedShow || matchedCompare) {
                this.newOffset = match.index;
            }
            // Return object, representing matched values
            if (matchedShow) {
                return [new Stock(matchedShow[3], matchedShow[4])];
            }
            if (matchedCompare) {
                return [new Stock(matchedCompare[3], matchedCompare[5]),
                new Stock(matchedCompare[4], matchedCompare[5])];
            }
        }
    }

    colorTextInput(htmlText) {
        replacements.forEach((key, value) => {
            htmlText = htmlText.replace(value, key);
        });
        return htmlText;
    }

    setCaretToEnd(target) {
        target.focus();
        document.execCommand("selectAll", false, null);
        document.getSelection().collapseToEnd();
    }
}

export default Input;