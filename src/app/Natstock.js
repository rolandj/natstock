import React, { Component } from 'react';
import Input from './input/Input';
import Instruction from './instruction/Instruction';
import Chart from './charts/Chart';
import './Natstock.css';


class Natstock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            symbols: "",
            firstPrices: "",
            firstDates: "",
            secondPrices: "",
            secondDates: "",
        };
        this.buildChart = this.buildChart.bind(this);
        this.processResponse = this.processResponse.bind(this);
    }

    render() {
        return (
            <div>
                <div className="background-container" >
                    <header>
                        <h1 className="logo-text">NatStock</h1>
                    </header>
                    <Input onPatternMatch={this.buildChart} />
                    <Instruction />
                </div >
                <div className="charts">
                    <Chart symbols={this.state.symbols} firstPrices={this.state.firstPrices}
                        firstDates={this.state.firstDates} secondPrices={this.state.secondPrices}
                        secondDates={this.state.secondDates} />
                </div>
            </div>
        );
    }

    /* Create chart: fetch API first then process response */
    buildChart(stocks) {
        const mode = stocks[0].getTimeMode();
        const fetchedData = [];
        const promises = [];
        stocks.forEach(element => {
            promises.push(element.fetchApi().then((response) => {
                fetchedData.push(response);
            }));
        });
        Promise.all(promises).then(() => {
            this.processResponse(fetchedData, mode);
        });
    }

    /* Unpack response JSON */
    processResponse(stocks, mode) {
        const firstPrices = [];
        const firstDates = [];
        const secondPrices = [];
        const secondDates = [];
        let symbols = [stocks[0]["Meta Data"]["2. Symbol"], mode[1]];
        const firstStock = stocks[0][mode[0]];

        for (let date in firstStock) {
            if (firstStock.hasOwnProperty(date)) {
                let price = firstStock[date];
                if (price.hasOwnProperty("4. close")) {
                    firstDates.push(date);
                    firstPrices.push(price["4. close"]);
                }
            }
        }
        if (stocks.length > 1) {
            // Unpack second stock data
            symbols = [
                stocks[0]["Meta Data"]["2. Symbol"],
                stocks[1]["Meta Data"]["2. Symbol"],
                mode[1]];
            const secondStock = stocks[1][mode[0]];

            for (let date in secondStock) {
                if (secondStock.hasOwnProperty(date)) {
                    let price = secondStock[date];
                    if (price.hasOwnProperty("4. close")) {
                        secondDates.push(date);
                        secondPrices.push(price["4. close"]);
                    }
                }
            }
        }
        this.setState({
            symbols: symbols,
            firstPrices: firstPrices,
            firstDates: firstDates,
            secondPrices: secondPrices,
            secondDates: secondDates,
        });
    }
}

export default Natstock;