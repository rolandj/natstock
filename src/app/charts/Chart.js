import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import './Chart.css';


class Chart extends Component {
    constructor(props) {
        super(props);
        this.drawChart = this.drawChart.bind(this);
        this.formatData = this.formatData.bind(this);
    }

    componentDidMount() {
        am4core.useTheme(am4themes_animated);
        this.chartContainer = am4core.create("chart-container", am4core.Container);
        this.chartContainer.width = am4core.percent(100);
        this.chartContainer.height = am4core.percent(100);
        this.chartContainer.layout = "vertical";
    }

    render() {
        if (this.props.firstPrices) {
            this.drawChart(
                this.props.symbols,
                this.props.firstPrices,
                this.props.firstDates,
                this.props.secondPrices,
                this.props.secondDates
            );
        }
        return (
            <div className="chart-container"></div>
        );
    }

    drawChart(symbols, firstPrices, firstDates, secondPrices, secondDates) {
        am4core.ready(() => {
            let chart = this.chartContainer.createChild(am4charts.XYChart);
            // Format data
            const chartData = this.formatData(firstPrices, firstDates, secondPrices, secondDates);
            chart.data = chartData;
            // Create axes
            chart.xAxes.push(new am4charts.DateAxis());
            chart.yAxes.push(new am4charts.ValueAxis());
            // Set title
            const title = chart.titles.create();
            title.text = `${symbols[0]} ${symbols[1]}`;
            title.fontSize = 30;
            title.marginBottom = 25;
            // Create series
            const series1 = chart.series.push(new am4charts.LineSeries());
            series1.dataFields.valueY = "value";
            series1.dataFields.dateX = "date";
            series1.tooltipText = `{date} : {value} [bold]${symbols[0]}`;
            series1.tooltip.pointerOrientation = "vertical";
            if (secondPrices.length) {
                const series2 = chart.series.push(new am4charts.LineSeries());
                series2.dataFields.valueY = "value1";
                series2.dataFields.dateX = "date1";
                series2.tooltipText = `{date1} : {value1} [bold] ${symbols[1]}`;
                series2.tooltip.pointerOrientation = "vertical";
                title.text = `${symbols[0]} ${symbols[1]} ${symbols[2]}`;
            }
            // Setup cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "none";
        });
    }

    formatData(firstPrices, firstDates, secondPrices, secondDates) {
        const chartData = [];
        for (let i = 0; i < firstPrices.length; i++) {
            if (secondPrices) {
                chartData.push({
                    date: firstDates[i],
                    value: firstPrices[i],
                    date1: secondDates[i],
                    value1: secondPrices[i]
                });
            } else {
                chartData.push({
                    date: firstDates[i],
                    value: firstPrices[i]
                });
            }
        }
        return chartData;
    }
}

export default Chart;