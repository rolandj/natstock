import React from 'react';
import './Instruction.css'


function Instructions(props) {
    return (
        <div className="instruction-container">
            <div className="example">
                <h3>Examples:</h3>
                <p>Do nothing, then show google by day.</p>
                <p>Let's compare microsoft to amazon by week and then maybe show tesla by month.</p>
            </div>
            <div className="flex-container">
                <div className="commands">
                    <h3>Commands:</h3>
                    <p>show &lt;company&gt; by &lt;interval&gt; .</p>
                    <p>compare &lt;company&gt; to &lt;company&gt; by &lt;interval&gt; .</p>
                </div>
                <div className="intervals">
                    <h3>Time intervals:</h3>
                    <p>day, week, month </p>
                </div>
                <div className="companies">
                    <h3>Companies: </h3>
                    <p>amazon, apple, facebook, google, netflix, tesla, microsoft, intel</p>
                </div>
            </div>
        </div>
    )
}

export default Instructions