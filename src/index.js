import React from 'react';
import ReactDOM from 'react-dom';
import Natstock from './app/Natstock';
import './index.css';


ReactDOM.render(
  <React.StrictMode>
    <Natstock />
  </React.StrictMode>,
  document.getElementById('root')
);